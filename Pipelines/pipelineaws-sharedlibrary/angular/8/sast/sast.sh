#!/bin/bash
if [ -z "$PROJ_ID" ]; then

echo "

#######################################################################################################

Erro : 335 - Aplicação $1 não cadastrada no fortify
Valide com o seu arquiteto de solução o cadastro dessa aplicação na ferramenta BOH

Caso a aplicação esteja cadastrada na ferramenta BOH e o erro persistir,
abrir chamado para a equipe responsável!
https://jiracorp.ctsp.prod.cloud.ihf/servicedesk/customer/portal/21/create/300

#######################################################################################################

" && exit 1; fi

cd $CODEBUILD_SRC_DIR
echo Build started on `date`

sourceanalyzer -b app-aws ./**/*
sourceanalyzer -b app-aws -scan -f fortify.fpr

echo "Fazendo upload ao fortify"
fortifyclient uploadFPR -url $ssc_url -f fortify.fpr -application $Sigla -applicationVersion $SiglaApp -authtoken $uploadtoken

sleep 20

echo "Buscando vulnerabilidades..."
VULS=`curl --insecure -s -X GET "${ssc_url}api/v1/projectVersions/$PROJ_ID/issues?filterset=a243b195-0a59-3f8b-1403-d55b7a7d78e6&filter=FOLDER%3Ab968f72f-cc12-03b5-976e-ad4c13920c21,FOLDER%3A5b50bb77-071d-08ed-fdba-1213fa90ac5a,CUSTOMTAG%5B87f2364f-dcd4-49e6-861d-f8d3f351686b%5D:,CUSTOMTAG%5B87f2364f-dcd4-49e6-861d-f8d3f351686b%5D:4,CUSTOMTAG%5B87f2364f-dcd4-49e6-861d-f8d3f351686b%5D:3,CUSTOMTAG%5B87f2364f-dcd4-49e6-861d-f8d3f351686b%5D:2,CUSTOMTAG%5B87f2364f-dcd4-49e6-861d-f8d3f351686b%5D:1&filterset=a243b195-0a59-3f8b-1403-d55b7a7d78e6" -H "authorization:FortifyToken ${apitoken}" -H "accept:application/json" | grep -Po '"count":\d*' | cut -d':' -f2`


if [ "$VULS" == "0" ]
then
   echo "

#######################################################################################################

Nenhuma vulnerabilidade encontrada!

#######################################################################################################

   "
else
   echo "

#######################################################################################################

Encontrada $VULS vulerabilidades criticas!

Verifique relatorio em ${ssc_url}html/ssc/version/$PROJ_ID/fix/null/

#######################################################################################################

   " && exit 1;
fi
