#!/bin/bash

PREFIX='AWS ::'
PROJECT=$featurename':'$microservicename
PROJECTKEY='aws-'$PROJECT
PROJECTNAME=$(echo $PREFIX" "$PROJECT)

echo "

               ##########################################################################
               #                                                                        #
               #                   INICIANDO A EXECUCAO DO SONAR!                       #
               #                                                                        #
               ##########################################################################

"
/sonar-scanner-3.3.0.1492-linux/bin/sonar-scanner -Dsonar.host.url=$SONAR_HOST -Dsonar.login=$SONAR_TOKEN -Dsonar.projectName="${PROJECTNAME}" -Dsonar.projectKey="${PROJECTKEY}" -Dsonar.sourceEncoding=UTF-8  -Dsonar.javascript.jstest.reportsPath=${CODEBUILD_SRC_DIR}/app/coverage -Dsonar.javascript.lcov.reportPaths=${CODEBUILD_SRC_DIR}/app/coverage/lcov.info -Dsonar.tests=${CODEBUILD_SRC_DIR}/app/src -Dsonar.test.inclusions=**/*.spec.* -Dsonar.typescript.lcov.reportPaths=${CODEBUILD_SRC_DIR}/app/coverage/lcov.info -Dsonar.sources=${CODEBUILD_SRC_DIR}/app/src/ -Dsonar.cfamily.build-wrapper-output.bypass=true -Dsonar.exclusions=**/environments/**,**/karma.conf.js,**/polyfills.js,**/node_modules/**,src/main.ts

rc=$?
if [ $rc -ne 0 ]; then
   echo [ERROR] exit code [$rc]; exit $rc
fi

sh ${CODEBUILD_SRC_DIR_Libs}/common/sonarqube/getStatusProjectStatus.sh "${PROJECTKEY}"
exit $?
