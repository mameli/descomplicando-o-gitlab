#!/bin/bash

account_name=$1
parameters=$2

bucketname=$(cat $parameters | jq -r '.Parameters.BucketName')

echo 'bucket name'
echo $bucketname

isInFile=$(cat $parameters | grep -c "DeployRole")
if [ $isInFile -eq 0 ]
then
   #string not contained in file
   role="arn:aws:iam::${account_name}:role/DevOpsRole"
else
   #string is in file at least once
   pipe_role=$(cat $parameters | jq -r '.Parameters.DeployRole')
   role="arn:aws:iam::${account_name}:role/${pipe_role}"
fi

response=$(aws sts assume-role --role-arn $role --role-session-name "cloudformation-access-${pipeline_environment}")
if [ "$?" -eq "0" ];
then
   echo 'Realizado o assume role'
else
   echo "

         #####################################################################
         #                                                                   #
         #  ERRO AO REALIZAR ASSUME ROLE, FAVOR VERIFICAR SE A ROLE EXISTE   #
         #                                                                   #
         #####################################################################

   "
   exit 1
fi

accessKey=$(echo $response | jq -r '.Credentials.AccessKeyId')
secret=$(echo $response | jq -r '.Credentials.SecretAccessKey')
token=$(echo $response | jq -r '.Credentials.SessionToken')
export AWS_ACCESS_KEY_ID=$accessKey
export AWS_SECRET_ACCESS_KEY=$secret
export AWS_SESSION_TOKEN=$token
aws s3 sync ${CODEBUILD_SRC_DIR_Build}/app/dist/. s3://${bucketname} --sse=aws:kms
