# Auditoria de CloudFormation na Pipeline

Objetivo: Auditar os templates de cloudFormation

> Software utilizado para auditoria [cfn_nag](https://github.com/stelligent/cfn_nag)

Resources verificados:

|Resource|Descrição|
|--------|---------|
|AWS::Logs::LogGroup|Necessário LogGroups no CloudwatchLogs|
|AWS::Logs::MetricFilter|Necessário criação de de filtros e métricas|
|AWS::CloudWatch::Alarm|Alarme|
|{{resolve:ssm:/org/member/workload_local_sns_arn:1}}|Apontamento do alarme para o Parameter Store da Landing Zone que tem o ARN do SNS|


CustomRules criados:

|Rules|Return Code|Message|
|-----|-----------|-------|
|[CloudLogGroupRule.rb](rules/CloudLogGroupRule.rb)|F5001|Resource Not Found|
|[CloudLogMetricFilterRule.rb](rules/CloudLogMetricFilterRule.rb)|F5002|Resource Not Found|
|[CloudWatchAlarmRule.rb](rules/CloudWatchAlarmRule.rb)|F5000|Resource Not Found|

