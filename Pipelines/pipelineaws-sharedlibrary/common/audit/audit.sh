#!/bin/bash

DIRECTORY="$CODEBUILD_SRC_DIR_App/infra"
touch audit.txt

if [ -d "$DIRECTORY" ]
then
   cfn_nag_scan -r $CODEBUILD_SRC_DIR/common/audit/rules -i $DIRECTORY/ -t ..*.yaml\|..*.yml >audit.txt
   EXIT=$?
   cat audit.txt
else
   echo "Diretório $DIRECTORY não existe"
   EXIT=0
fi
exit $EXIT
