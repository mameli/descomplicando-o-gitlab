import json
import os
from os import path
import boto3

def load(file):
    if path.exists(file):
        j_file = open(file,'r')
        raw = j_file.read()
        j_file.close()
        try:
          playload = json.loads(raw)
          print("Arquivo do parameters carregado")
        except ValueError as error:
          print("Problema para carregar arquivo {0}: {1}".format(file,error))
          playload = {'Parameters':{}}
    else:
       print("Arquivos de parameters nao existe")
       playload = {'Parameters':{}}

    playload['Parameters']["FeatureName"] = os.environ['FeatureName']
    playload['Parameters']["MicroServiceName"] = os.environ['MicroServiceName']
    return playload


def getTags():
    tags = {}
    ssm = boto3.client('ssm')
    try:
      params = ssm.get_parameters_by_path(Path='/Tags')
      tags = params['Parameters']
    except ValueError as error:
      print("Problema ao carregar Tags do SSM {0}".format(error))
    return tags


def addTags(playload):
    tags = getTags()

    playTags = {
        "NomeProduto" : os.environ['FeatureName'],
        "MicroService": os.environ['MicroServiceName']
    }

    for tag in tags:
        playTags[tag['Name']] = tag['Value']

    playload['Tags'] = playTags
    return playload


def savefile(file,playload):
    try:
      j_file = open(file,'w')
      j_file.write(json.dumps(playload))

      j_file.close()
      return True
    except ValueError as error:
      print("Problema para salvar arquivo {0}: {1}".format(file,error))
      return False


if __name__ == '__main__':
    path_file = os.environ['CODEBUILD_SRC_DIR_App']
    files = [
         "{0}/infra/parameters-dev.json".format(path_file),
         "{0}/infra/parameters-hom.json".format(path_file),
         "{0}/infra/parameters-prod.json".format(path_file)
    ]

    for file in files:
        playload_temp = load(file)
        playload = addTags(playload_temp)
        print("{0}: {1}".format(file,playload))
        if not savefile(file,playload):
            exit(1)

    exit(0)
