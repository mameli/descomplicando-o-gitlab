#!/bin/bash

cd $CODEBUILD_SRC_DIR_App/app/
cp /microscanner ./
echo -e "\r\n" >>Dockerfile
echo "COPY /microscanner ." >>Dockerfile
echo "USER 0" >>Dockerfile
echo "RUN chmod +x microscanner" >>Dockerfile
echo "RUN ./microscanner $TOKEN_AQUA --continue-on-failure --html" >>Dockerfile
docker build --no-cache . >>results.html
sed -n '/<html/,/<\/html/p' results.html >>report_aqua.html
echo "Copying report to s3..."
aws s3 cp report_aqua.html s3://$s3bucket/$CODEBUILD_BUILD_ID/report_aqua.html
echo "Generate report link..."
report="s3://$s3bucket/$CODEBUILD_BUILD_ID/report_aqua.html"
echo $report
body='{"result":{"value":"'$report'", "accountid":"'$AccountID'", "variablepath":"'$VariablePath'", "type":"Aqua"}}'
echo $body
aws sqs send-message --queue-url $queue --message-body "$body"

