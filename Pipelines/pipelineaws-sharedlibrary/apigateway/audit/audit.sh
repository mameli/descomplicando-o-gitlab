#!/bin/bash

DIRECTORY="${CODEBUILD_SRC_DIR}/AWS"
touch audit.txt

if [ -d "$DIRECTORY" ]
then
   #cfn_nag_scan -r $CODEBUILD_SRC_DIR_Libs/iac/cloudformation/audit/resource_policy -i $DIRECTORY/ -t ..*.yaml\|..*.yml >audit.txt
   cfn_nag_scan -r $CODEBUILD_SRC_DIR_Libs/apigateway/audit/rules -i $DIRECTORY/ -t ..*.yaml\|..*.yml >audit.txt
   EXIT=$?
   #EXIT=0
   cat audit.txt
else
   echo "Diretório $DIRECTORY não existe"
   EXIT=0
fi
exit $EXIT
