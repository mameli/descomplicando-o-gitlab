#!/bin/bash

DEVTOOL=573583577864 #demomvp - too

FEATURE_NAME="reestruturacao"
MICROSERVICE_NAME="deploydockerecs"
RUNTIME="imagem docker"
RESPONSAVEL="paheneb"
TEMPLATE_FILE="./pipeline-deploy-imagem-docker-ecs.yml"

DevSecOpsLambdaCriaCardImplantacao="DevSecOps-CriaCardImplantacao"
DevSecOpsLambdaCriaMudancaAgendada="DevSecOps-CriaMudancaAgendada"
DevSecOpsLambdaVerificaGmudLiberada="DevSecOps-VerificaGmudLiberada"
DevSecOpsLambdaEncerraMudanca="DevSecOps-EncerraMudanca"

~/.aws/login-account.sh $DEVTOOL demomvptoo

aws cloudformation deploy --profile demomvptoo --template-file $TEMPLATE_FILE --capabilities CAPABILITY_IAM --stack-name StackSet-Pipeline-$FEATURE_NAME-$MICROSERVICE_NAME-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME" "Runtime=$RUNTIME"

echo "deploy develop done"

aws cloudformation deploy --profile demomvptoo --template-file $TEMPLATE_FILE --capabilities CAPABILITY_IAM --stack-name StackSet-Pipeline-$FEATURE_NAME-$MICROSERVICE_NAME-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME" "Runtime=$RUNTIME" "DevSecOpsLambdaCriaCardImplantacao=$DevSecOpsLambdaCriaCardImplantacao" "DevSecOpsLambdaCriaMudancaAgendada=$DevSecOpsLambdaCriaMudancaAgendada" "DevSecOpsLambdaVerificaGmudLiberada=$DevSecOpsLambdaVerificaGmudLiberada" "DevSecOpsLambdaEncerraMudanca=$DevSecOpsLambdaEncerraMudanca"

echo "deploy master done"
