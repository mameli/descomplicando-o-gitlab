#!/bin/bash

DEVTOOL=344424632347 #demomvp - too

~/.aws/login-account.sh $DEVTOOL pipelinestoo

stack_to_update="Check-Dotnet-31-ECS-Develop Check-Dotnet-31-ECS-Master Check-Dotnet-21-ECS-Master Check-Dotnet-21-ECS-Develop check-maven-openjdk8-ecs-develop check-maven-openjdk8-ecs-master check-maven-openjdk11-ecs-develop check-maven-openjdk11-ecs-master check-gradle-openjdk8-ecs-develop check-gradle-openjdk8-ecs-master check-gradle-openjdk11-ecs-develop check-gradle-openjdk11-ecs-master check-python-3-7-ecs-develop check-python-3-7-ecs-master check-nodejs-10-ecs-develop check-nodejs-10-ecs-master"

IFS=' ' # hyphen (-) is set as delimiter
read -ra stack_to_update_arr <<< "$stack_to_update" 

for stack_name in "${stack_to_update_arr[@]}"; do 

    echo $stack_name

    aws cloudformation update-stack --profile pipelinestoo --stack-name $stack_name --template-url https://344424632347-pipeline-tamplates.s3-sa-east-1.amazonaws.com/pipeline_app_ecs_4.0.0.yml --parameters ParameterKey=FeatureName,UsePreviousValue=true ParameterKey=MicroServiceName,UsePreviousValue=true ParameterKey=AprovadorBO,UsePreviousValue=true ParameterKey=AprovadorPO,UsePreviousValue=true ParameterKey=BranchName,UsePreviousValue=true ParameterKey=BuildCustomizado,UsePreviousValue=true ParameterKey=ClusterName,UsePreviousValue=true ParameterKey=NamespaceGitLab,UsePreviousValue=true ParameterKey=Runtime,UsePreviousValue=true ParameterKey=RepositorioGitLab,UsePreviousValue=true

done
