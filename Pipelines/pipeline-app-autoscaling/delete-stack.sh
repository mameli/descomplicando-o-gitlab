#!/bin/bash

branch=$1
MICROSERVICE_NAME=$2
ENVIRONMENT=$3
STACK_NAME=$4
TYPE=$5

echo $TYPE
if [ "$TYPE" = "DEVTOOL" ]; then
    alias aws='docker run --env-file ./variables -v $(pwd):/aws amazon/aws-cli'
else
    alias aws='docker run --env-file ./variableswkl_$ENVIRONMENT -v $(pwd):/aws amazon/aws-cli'
fi

aws cloudformation delete-stack --stack-name $STACK_NAME
aws cloudformation describe-stacks --region sa-east-1 --stack-name $STACK_NAME
resource_status=$(aws cloudformation describe-stacks --region sa-east-1 --stack-name $STACK_NAME)
status=$(echo $?)
echo $status
resourcestatus=$(echo $resource_status | jq -r '.Stacks[0].StackStatus')

while [ $status -eq 0 ]
do
    if [ "$resourcestatus" = "DELETE_IN_PROGRESS" ]; then
        sleep 60
        echo $resourcestatus
        resource_status=$(aws cloudformation describe-stacks --stack-name $STACK_NAME --region sa-east-1 2>&1)
        status=$(echo $?)
        resourcestatus=$(echo $resource_status | jq -r '.Stacks[0].StackStatus' 2>&1)
        echo $status
    elif [ "$resourcestatus" = "DELETE_FAILED" ]; then
        echo 'Erro ao deletar a stack'
        exit 1
        break
    fi        
done

echo 'Stack Deletada com sucesso'
