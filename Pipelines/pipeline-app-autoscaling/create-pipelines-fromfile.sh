#!/bin/bash

DEVTOOL=573583577864 #demomvp - too

FEATURE_NAME="asgstandard"
RESPONSAVEL="paheneb"

~/.aws/login-account.sh $DEVTOOL demomvptoo

########################################################################
MICROSERVICE_NAME_MAVENONZE="mavenopenjdkonze"
RUNTIME_MAVENONZE="maven: openjdk11"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_MAVENONZE-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_MAVENONZE" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME" "Runtime=$RUNTIME_MAVENONZE"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_MAVENONZE-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_MAVENONZE" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_MAVENONZE" "Runtime=$RUNTIME_MAVENONZE"

# echo "deploy master done"

########################################################################
MICROSERVICE_NAME_MAVENOITO="mavenopenjdkoito"
RUNTIME_MAVENOITO="maven: openjdk8"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_MAVENOITO-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_MAVENOITO" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_MAVENOITO" "Runtime=$RUNTIME_MAVENOITO"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_MAVENOITO-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_MAVENOITO" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_MAVENOITO" "Runtime=$RUNTIME_MAVENOITO"

########################################################################
MICROSERVICE_NAME_GRADLEOITO="gradlejdkoito"
RUNTIME_GRADLEOITO="gradle: openjdk8"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_GRADLEOITO-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_GRADLEOITO" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_GRADLEOITO" "Runtime=$RUNTIME_GRADLEOITO"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_GRADLEOITO-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_GRADLEOITO" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_GRADLEOITO" "Runtime=$RUNTIME_GRADLEOITO"

########################################################################
MICROSERVICE_NAME_GRADLEONZE="gradlejdkonze"
RUNTIME_GRADLEONZE="gradle: openjdk11"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_GRADLEONZE-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_GRADLEONZE" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_GRADLEOITO" "Runtime=$RUNTIME_GRADLEONZE"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_GRADLEONZE-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_GRADLEONZE" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_GRADLEOITO" "Runtime=$RUNTIME_GRADLEONZE"

########################################################################
MICROSERVICE_NAME_DOTNET21="dotnetdoisum"
RUNTIME_DOTNET21="dotnet: 2.1"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_DOTNET21-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_DOTNET21" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_DOTNET21" "Runtime=$RUNTIME_DOTNET21"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_DOTNET21-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_DOTNET21" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_DOTNET21" "Runtime=$RUNTIME_DOTNET21"

########################################################################
MICROSERVICE_NAME_DOTNET31="dotnettresum"
RUNTIME_DOTNET31="dotnet: 3.1"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_DOTNET31-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_DOTNET31" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_DOTNET31" "Runtime=$RUNTIME_DOTNET31"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_DOTNET31-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_DOTNET31" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_DOTNET31" "Runtime=$RUNTIME_DOTNET31"

########################################################################
MICROSERVICE_NAME_PYTHON="python"
RUNTIME_PYTHON="python: 3.7"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_PYTHON-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_PYTHON" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_PYTHON" "Runtime=$RUNTIME_PYTHON"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_PYTHON-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_PYTHON" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_PYTHON" "Runtime=$RUNTIME_PYTHON"

########################################################################
MICROSERVICE_NAME_NODE="nodejs"
RUNTIME_NODE="nodejs: 10"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_NODE-develop --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=develop" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_NODE" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_NODE" "Runtime=$RUNTIME_NODE"

aws cloudformation deploy --profile demomvptoo --template-file ./pipeline-app-asg.yml --capabilities CAPABILITY_IAM --stack-name Pipeline-R3-S4-Ouver-$FEATURE_NAME-$MICROSERVICE_NAME_NODE-master --parameter-overrides "AprovadorBO=$RESPONSAVEL@itau-unibanco.com.br" "AprovadorPO=$RESPONSAVEL@itau-unibanco.com.br" "BranchName=master" "BuildCustomizado=false" "FeatureName=$FEATURE_NAME" "MicroServiceName=$MICROSERVICE_NAME_NODE" "NamespaceGitLab=cloud-publica-devsecops" "RepositorioGitLab=$FEATURE_NAME-$MICROSERVICE_NAME_NODE" "Runtime=$RUNTIME_NODE"

########################################################################
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_MAVENONZE-develop
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_MAVENONZE-master

aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_MAVENOITO-develop
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_MAVENOITO-master

aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_DOTNET21-develop
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_DOTNET21-master

aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_GRADLEONZE-develop
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_GRADLEONZE-master

aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_GRADLEOITO-develop
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_GRADLEOITO-master

aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_DOTNET31-develop
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_DOTNET31-master

aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_PYTHON-develop
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_PYTHON-master

aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_NODE-develop
aws codepipeline start-pipeline-execution --profile demomvptoo --name $FEATURE_NAME-$MICROSERVICE_NAME_NODE-master
