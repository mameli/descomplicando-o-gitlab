#!/bin/bash

DEVTOOL=344424632347 #demomvp - too

~/.aws/login-account.sh $DEVTOOL pipelinestoo

stack_to_update="Check-Dotnet-31-ASG-Develop Check-Dotnet-31-ASG-Master Check-Dotnet-21-ASG-Master Check-Dotnet-21-ASG-Develop check-maven-openjdk8-ASG-develop check-maven-openjdk8-ASG-master check-maven-openjdk11-ASG-develop check-maven-openjdk11-ASG-master check-gradle-openjdk8-ASG-develop check-gradle-openjdk8-ASG-master check-gradle-openjdk11-ASG-develop check-gradle-openjdk11-ASG-master check-python-3-7-ASG-develop check-python-3-7-ASG-master check-nodejs-10-ASG-develop check-nodejs-10-ASG-master"

IFS=' ' # hyphen (-) is set as delimiter
read -ra stack_to_update_arr <<< "$stack_to_update" 

for stack_name in "${stack_to_update_arr[@]}"; do 

    echo $stack_name

    aws cloudformation update-stack --profile pipelinestoo --stack-name $stack_name --template-url https://344424632347-pipeline-tamplates.s3-sa-east-1.amazonaws.com/pipeline_app_asg_1.0.0.yml --parameters ParameterKey=FeatureName,UsePreviousValue=true ParameterKey=MicroServiceName,UsePreviousValue=true ParameterKey=AprovadorBO,UsePreviousValue=true ParameterKey=AprovadorPO,UsePreviousValue=true ParameterKey=BranchName,UsePreviousValue=true ParameterKey=BuildCustomizado,UsePreviousValue=true ParameterKey=ClusterName,UsePreviousValue=true ParameterKey=NamespaceGitLab,UsePreviousValue=true ParameterKey=Runtime,UsePreviousValue=true ParameterKey=RepositorioGitLab,UsePreviousValue=true

done
