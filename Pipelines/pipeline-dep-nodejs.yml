AWSTemplateFormatVersion: "2010-09-09"
Description: Pipeline responsável por publicar dependências NodeJS no Artifactory | Versao 1.0.1
Parameters:
  
  PipelineVersion:
    Description: Versao de utilizacao da pipeline.
    Type: String
    Default: 1.0.1
  
  FeatureName:
    Description: >-
      Nome do produto, necessario refletir a taxonomia utilizada para a Cloud
      Publica AWS Itau.
    Type: String
    AllowedPattern: '[a-z]*'
  
  MicroServiceName:
    Description: >-
      Nome do microsservico ou aplicacao, necessario refletir a taxonomia
      utilizada para a Cloud Publica AWS Itau.
    Type: String
    AllowedPattern: '[a-z]*'
  
  Runtime:
    Description: Runtime do codigo da aplicacao.
    Type: String
    Default: 'nodejs: nvm'
    AllowedValues:
      - 'nodejs: nvm'
  
  NamespaceGitLab:
    Description: Nome do espaco do GitLab Corporativo do produto (Ex. Sigla).
    Type: String
    Default: <namespace>
  
  DevSecOpsAccount:
    Description: Numero da conta core da squad DevSecOps.
    Type: 'AWS::SSM::Parameter::Value<String>'
    Default: /Accounts/DevSecOps
  
  DevAccount:
    Description: Nome da conta de dev
    Default: /Accounts/Dev
    Type: 'AWS::SSM::Parameter::Value<String>'
  
  HomologAccount:
    Description: Nome da conta de dev
    Default: /Accounts/Homolog
    Type: 'AWS::SSM::Parameter::Value<String>'
  
  ProdAccount:
    Description: Numero da conta de Producao do produto.
    Type:  'AWS::SSM::Parameter::Value<String>'
    Default: '/Accounts/Prod'
 
  DevToolsAccount:
    Description: Nome da conta de Devtools
    Default: /Accounts/DevTools
    Type: 'AWS::SSM::Parameter::Value<String>'
  
  BranchName:
    Description: 'Nome da branch, necessario escolher entre "master" ou "develop".'
    Type: String
    Default: master
  
  RepositorioGitLab:
    Description: Nome do repositorio do Gitlab Corporativo do microsservico ou aplicacao.
    Type: String
    Default: <repositorio>
  
  KMSKeyArn:
    Description: Nome do recurso na AWS para a chave KMS da pipeline.
    Type: 'AWS::SSM::Parameter::Value<String>'
    Default: /Shared/KMSKeyArn
  
  VPCID:
    Description: Numero de identificacao da VPC (Virtual Private Cloud) da conta.
    Type: 'AWS::SSM::Parameter::Value<String>'
    Default: /Networking/VPCID
  
  PrivateSubnetOne:
    Description: Numero de identificacao da primeira sub-rede Privada da conta.
    Type: 'AWS::SSM::Parameter::Value<String>'
    Default: /Networking/PrivateSubnetOne
  
  PrivateSubnetTwo:
    Description: Numero de identificacao da segunda sub-rede Privada da conta.
    Type: 'AWS::SSM::Parameter::Value<String>'
    Default: /Networking/PrivateSubnetTwo

Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
      - Label:
          default: REQUIRED FIELDS
        Parameters:
          - FeatureName
          - MicroServiceName
          - NamespaceGitLab
          - Runtime
          - RepositorioGitLab
          - BranchName
      - Label:
          default: DO NOT MAKE CHANGES ON THE PARAMETERS BELOW
        Parameters:
          - DevSecOpsAccount
          - DevToolsAccount
          - ProdAccount
          - HomologAccount
          - DevAccount
          - PrivateSubnetOne
          - PrivateSubnetTwo
          - VPCID
          - KMSKeyArn
Conditions:
  BranchMaster: !Equals [ "master", !Ref BranchName ]
  BranchDevelop: !Equals [ "develop", !Ref BranchName ]

############################## RESOURCES
Resources:

  CloudEventRuleSource:
    Type: 'AWS::Events::Rule'
    Properties:
      Description: Event Rule CodePipeline
      EventPattern:
        source:
          - aws.codecommit
        detail-type:
          - CodeCommit Repository State Change
        resources:
          - !Sub >-
            arn:aws:codecommit:${AWS::Region}:${AWS::AccountId}:${FeatureName}-${MicroServiceName}
        detail:
          event:
            - referenceCreated
            - referenceUpdated
          referenceType:
            - branch
          referenceName:
            - !Sub '${BranchName}'
      State: ENABLED
      Targets:
        - Arn: !Sub >-
            arn:aws:codepipeline:${AWS::Region}:${AWS::AccountId}:${FeatureName}-${MicroServiceName}-${BranchName}
          Id: !Sub '${FeatureName}${MicroServiceName}'
          RoleArn: !Sub >-
            arn:aws:iam::${DevToolsAccount}:role/rangers-start-pipeline-execution-role
    DependsOn:
      - Pipeline

  ############################## CodePipeline (Estrutura da Pipeline)    
  Pipeline:
    Type: 'AWS::CodePipeline::Pipeline'
    Properties:
      Tags:
        - Key: Cloudformation
          Value: !Ref 'AWS::StackName'
        - Key: Runtime
          Value: !Ref Runtime
        - Key: PipelineVersion
          Value: !Ref PipelineVersion
        - Key: PipelineType
          Value: pipeline-dep-nodejs

      Name: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', !Ref 'BranchName']]
      RoleArn: !Sub 'arn:aws:iam::${AWS::AccountId}:role/DevOpsRole'
      ArtifactStore:
        Type: S3
        Location: !Sub '${AWS::AccountId}-artefatos'
        EncryptionKey:
          Id: !Ref KMSKeyArn
          Type: KMS
      
      Stages:
        - Name: Source
          Actions:
            - Name: App
              ActionTypeId:
                Category: Source
                Owner: AWS
                Version: '1'
                Provider: CodeCommit
              Configuration:
                RepositoryName: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName']]
                PollForSourceChanges: false
                BranchName: !Ref BranchName
              OutputArtifacts:
                - Name: App
              RunOrder: 1

            - Name: SharedLibrary
              ActionTypeId:
                Category: Source
                Owner: AWS
                Provider: CodeCommit
                Version: '1'
              Configuration:
                BranchName: release-4.11.x
                RepositoryName: pipelineaws-sharedlibrary
                PollForSourceChanges: false
              OutputArtifacts:
                - Name: Libs
              RunOrder: 1
              RoleArn: !Sub 'arn:aws:iam::${DevSecOpsAccount}:role/DevOpsRole'

        - Name: Continuous-Integration
          Actions:

            - Name: TestUnit
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: '1'
                Provider: CodeBuild
              OutputArtifacts:
                - Name: TestUnit
              InputArtifacts:
                - Name: App
                - Name: Libs
              Configuration:
                ProjectName: !Join ['-', [!Ref 'FeatureName',!Ref 'MicroServiceName', 'TestesUnitarios', !Ref 'BranchName' ]]
                PrimarySource: App
              RunOrder: 1

            - Name: Build
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: '1'
                Provider: CodeBuild
              OutputArtifacts:
                - Name: Build
              InputArtifacts:
                - Name: App
                - Name: Libs
              Configuration:
                ProjectName: !Join ['-', [!Ref 'FeatureName',!Ref 'MicroServiceName', 'Build', !Ref 'BranchName' ]]
                PrimarySource: App
              RunOrder: 1

            - Name: SonarQube
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: '1'
                Provider: CodeBuild
              OutputArtifacts:
                - Name: Sonar
              InputArtifacts:
                - Name: App
                - Name: Libs
              Configuration:
                ProjectName: !Join ['-', [!Ref 'FeatureName',!Ref 'MicroServiceName', 'Sonar', !Ref 'BranchName' ]]
                PrimarySource: App
              RunOrder: 1

            - Name: SAST-Fortify
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: '1'
                Provider: CodeBuild
              OutputArtifacts:
                - Name: Fortify
              InputArtifacts:
                - Name: App
                - Name: Libs
              Configuration:
                ProjectName: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'Fortify', !Ref 'BranchName' ]]
                PrimarySource: App
              RunOrder: 1

        - !If
          - BranchDevelop
          - Name: PublishDepDes
            Actions:
              - Name: PublishLibsArtifactoryDev
                ActionTypeId:
                  Category: Build
                  Owner: AWS
                  Version: "1"
                  Provider: CodeBuild
                OutputArtifacts:
                    - Name: PublishDev
                InputArtifacts:
                    - Name: App
                    - Name: Build
                    - Name: Libs
                Configuration:
                  ProjectName: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'PublishDepDes' ]]
                  PrimarySource: Libs
                RunOrder: 1
          - !Ref AWS::NoValue
        
        - !If
          - BranchMaster
          - Name: PublishDepProd
            Actions:
              - Name: PublishLibsArtifactory
                ActionTypeId:
                  Category: Build
                  Owner: AWS
                  Version: "1"
                  Provider: CodeBuild
                OutputArtifacts:
                    - Name: PublishProd
                InputArtifacts:
                    - Name: App
                    - Name: Build
                    - Name: Libs
                Configuration:
                  ProjectName: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'PublishDepProd' ]]
                  PrimarySource: Libs
                RunOrder: 1
          - !Ref AWS::NoValue

################### CODEBUILD
  BuildTestUnit:
    Type: 'AWS::CodeBuild::Project'
    Properties:
      Name: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'TestesUnitarios',  !Ref BranchName]]
      Description: Testes Unitarios
      EncryptionKey: !Ref KMSKeyArn
      ServiceRole: !Sub 'arn:aws:iam::${AWS::AccountId}:role/DevOpsRole'
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_LARGE
        Image: 'aws/codebuild/standard:5.0'
        PrivilegedMode: true
        EnvironmentVariables:
          - Name: DevSecOpsAccount
            Value: !Ref DevSecOpsAccount
      Source:
        Type: CODEPIPELINE
        BuildSpec: ../01/artifactory/dep/nodejs/testunit/buildspec.yml
      TimeoutInMinutes: 480
      Cache:
        Location: !Sub '${AWS::AccountId}-cache'
        Modes:
          - LOCAL_CUSTOM_CACHE
        Type: S3
      VpcConfig:
        VpcId: !Ref VPCID
        Subnets:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
        SecurityGroupIds:
          - !Ref SgPipeline

  Build:
    Type: 'AWS::CodeBuild::Project'
    Properties:
      Name: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'Build', !Ref BranchName ]]
      Description: Build da Aplicacao
      EncryptionKey: !Ref KMSKeyArn
      ServiceRole: !Sub 'arn:aws:iam::${AWS::AccountId}:role/DevOpsRole'
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        ImagePullCredentialsType: CODEBUILD
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_LARGE
        Image: 'aws/codebuild/standard:5.0'
        PrivilegedMode: true
        EnvironmentVariables:
          - Name: DevSecOpsAccount
            Value: !Ref DevSecOpsAccount
          - Name: BranchName
            Value: !Ref BranchName
      Source:
        Type: CODEPIPELINE
        BuildSpec: ../01/artifactory/dep/nodejs/build/buildspec.yml
      TimeoutInMinutes: 480
      Cache:
        Location: !Sub '${AWS::AccountId}-cache'
        Modes:
          - LOCAL_CUSTOM_CACHE
        Type: S3
      VpcConfig:
        VpcId: !Ref VPCID
        Subnets:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
        SecurityGroupIds:
          - !Ref SgPipeline

  BuildSonar:
    Type: 'AWS::CodeBuild::Project'
    Properties:
      Name: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'Sonar', !Ref BranchName ]]
      Description: Sonar
      EncryptionKey: !Ref KMSKeyArn
      ServiceRole: !Sub 'arn:aws:iam::${AWS::AccountId}:role/DevOpsRole'
      Cache:
        Location: !Sub '${AWS::AccountId}-cache'
        Modes:
          - LOCAL_CUSTOM_CACHE
        Type: S3
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        ImagePullCredentialsType: CODEBUILD
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_LARGE
        Image: 'aws/codebuild/standard:5.0'
        PrivilegedMode: true
        EnvironmentVariables:
          - Name: BranchName
            Value: !Ref BranchName
          - Name: s3bucket
            Value: !Sub '${AWS::AccountId}-reports'
          - Name: MicroServiceName
            Value: !Ref MicroServiceName
          - Name: FeatureName
            Value: !Ref FeatureName
      Source:
        Type: CODEPIPELINE
        BuildSpec: ../01/artifactory/dep/nodejs/sonarqube/buildspec.yml
      TimeoutInMinutes: 480
      VpcConfig:
        VpcId: !Ref VPCID
        Subnets:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
        SecurityGroupIds:
          - !Ref SgPipeline

  BuildSAST:
    Type: 'AWS::CodeBuild::Project'
    Properties:
      Name: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'Fortify', !Ref BranchName ]]
      Description: Fortify
      EncryptionKey: !Ref KMSKeyArn
      ServiceRole: !Sub 'arn:aws:iam::${AWS::AccountId}:role/DevOpsRole'
      Cache:
        Location: !Sub '${AWS::AccountId}-cache'
        Modes:
          - LOCAL_CUSTOM_CACHE
        Type: S3
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        ImagePullCredentialsType: CODEBUILD
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_LARGE
        Image: 'aws/codebuild/standard:5.0'
        PrivilegedMode: true
        EnvironmentVariables:
          - Name: BranchName
            Value: !Ref BranchName
          - Name: VariablePath
            Value: !Sub '/App/${FeatureName}/${MicroServiceName}/'
          - Name: AccountID
            Value: !Sub '${AWS::AccountId}'
          - Name: s3bucket
            Value: !Sub '${AWS::AccountId}-reports'
          - Name: queue
            Value: >-
              https://sqs.sa-east-1.amazonaws.com/049557819541/DevSecOps-ToolsReports
          - Name: microservicename
            Value: !Ref MicroServiceName
          - Name: featurename
            Value: !Ref FeatureName
          - Name: Runtime
            Value: !Ref Runtime
      Source:
        Type: CODEPIPELINE
        BuildSpec: ../01/common/sastasaservice/buildspec.yml
      TimeoutInMinutes: 480
      VpcConfig:
        VpcId: !Ref VPCID
        Subnets:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
        SecurityGroupIds:
          - !Ref SgPipeline
  
  PublishDev:
    Type: 'AWS::CodeBuild::Project'
    Condition: BranchDevelop
    Properties:
      Name: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'PublishDepDes' ]]
      Description: Deploy
      EncryptionKey: !Ref KMSKeyArn
      ServiceRole: !Sub 'arn:aws:iam::${AWS::AccountId}:role/DevOpsRole'
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_LARGE
        Image: 'aws/codebuild/standard:5.0'
        PrivilegedMode: true
        EnvironmentVariables:
          - Name: pipeline_environment
            Value: DEV
          - Name: ecr_account
            Value: !Ref DevToolsAccount
          - Name: FeatureName
            Value: !Ref FeatureName
          - Name: MicroServiceName
            Value: !Ref MicroServiceName
          - Name: BranchName
            Value: !Ref BranchName
      Source:
        Type: CODEPIPELINE
        BuildSpec: ../00/artifactory/dep/nodejs/publish/buildspec.yml
      TimeoutInMinutes: 480
      VpcConfig:
        VpcId: !Ref VPCID
        Subnets:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
        SecurityGroupIds:
          - !Ref SgPipeline

  PublishProd:
    Type: AWS::CodeBuild::Project
    Condition: BranchMaster
    Properties:
      Name: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', 'PublishDepProd' ]]
      Description: Deploy
      EncryptionKey: !Ref KMSKeyArn
      ServiceRole: !Sub 'arn:aws:iam::${AWS::AccountId}:role/DevOpsRole'
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_LARGE
        Image: aws/codebuild/standard:5.0
        PrivilegedMode: true
        EnvironmentVariables:
          - Name: pipeline_environment
            Value: PROD
          - Name: ecr_account
            Value: !Ref DevToolsAccount
          - Name: FeatureName
            Value: !Ref FeatureName
          - Name: MicroServiceName
            Value: !Ref MicroServiceName
          - Name: BranchName
            Value: !Ref BranchName
      Source:
        Type: CODEPIPELINE
        BuildSpec: ../00/artifactory/dep/nodejs/publish/buildspec.yml
      TimeoutInMinutes: 480
      VpcConfig:
        VpcId: !Ref VPCID
        Subnets:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
        SecurityGroupIds:
          - !Ref SgPipeline

  SgPipeline:
    Type: 'AWS::EC2::SecurityGroup'
    Metadata:
      cfn_nag:
        rules_to_suppress:
          - id: F1000
    Properties:
      VpcId: !Ref VPCID
      GroupName: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName', !Ref 'BranchName',  f]]
      GroupDescription: This security group is used to control access to the container

  RuntimeParameter:
    Type: 'AWS::SSM::Parameter'
    Condition: BranchDevelop
    Properties:
      Name: !Sub '/App/${FeatureName}/${MicroServiceName}/Runtime'
      Type: String
      Value: !Ref Runtime

  RepositorioGitLabParameter:
    Type: 'AWS::SSM::Parameter'
    Condition: BranchMaster
    Properties:
      Name: !Sub '/App/${FeatureName}/${MicroServiceName}/RepositorioGitLab'
      Type: String
      Value: !Ref RepositorioGitLab

  NamespaceGitLabParameter:
    Type: 'AWS::SSM::Parameter'
    Condition: BranchMaster
    Properties:
      Name: !Sub '/App/${FeatureName}/${MicroServiceName}/NamespaceGitLab'
      Type: String
      Value: !Ref NamespaceGitLab

  IC:
    Type: 'AWS::SSM::Parameter'
    Condition: BranchMaster
    Properties:
      Name: !Sub '/App/${FeatureName}/${MicroServiceName}/config_item'
      Type: String
      Value: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName']]

############################## CodeCommit e Gitlab - (Repositorio de codigo)
  CheckRepoExistsCustomResourceSns:
    Type: 'Custom::CheckRepoExistsCustomResourceLambda'
    Properties:
      ServiceToken: !Sub 'arn:aws:sns:sa-east-1:${DevSecOpsAccount}:DevSecOps-CheckRepoCodeCommitExists'
      RepositorioCodecommit: !Join ['-', [!Ref 'FeatureName', !Ref 'MicroServiceName']]
      AccountId: !Sub '${AWS::AccountId}'
      Runtime: !Ref Runtime
      NamespaceGitLab: !Ref NamespaceGitLab
      PipelineType: LIB
