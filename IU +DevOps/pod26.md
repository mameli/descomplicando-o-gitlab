## Workstream: 26. RT Processamentos - Integração
- Itau Workstream Tech Leader: Williams Gonçalves Cerqueira
- AWS Workstream Tech Leader: Chanki Nathani
- AWS Workstream Engagement Manager: Leonardo Volkmar
- Itau Workstream Engagement Manager: Diego Eduardo Marco
- Product Owner: Maria Alice Bueno
- Control Tower: Giovanna Carpi Rodrigues / Carlos Henrique Pacheco Souza
- Stakeholders: Carlos Eduardo Almeida Mazzei / Caio Cesar Lopes Gomes / Marcelo Nomura
- AWS TEAM:
  - CIA: Daniel Munoz
  - DevOps: Ricardo Salcedo
  - Advisory: Sergio Klarreich


## General Information
- **Sprints**
  - Sprint 27 - 01/nov até 19/nov (3 weeks)
  - Sprint 28 - 22/nov até 10/dez (3 weeks)
  - Sprint 29 - 13/dez até 31/dez (3 weeks)<br><br>

- **Cerimonies**
  - Sprint Planning - Every first day of the sprint (2 hours - 14:00 to 16:00 BRT)
  - Daily - Monday to Friday (15 minutes - 10:45 to 11:00 BRT)
  - Tech Daily standup is recommended
  - Demo - Last Day of the sprint (1,5 hours - 14:00 to 15:30 BRT)
  - Retrospective - Last Day of the Sprint (30 minutes - 15:30 to 16:00 BRT)<br><br>

- **Meeting Recordings Information:**
  - LRP Pre-Presentation Recording
  - LRP Presentation Recording


### Related documents
- Business Alignment Outcome Canvas, available here
- Prioritization Model, available here
- Community OKRs, <Insert Link>


## LRP Release Outcome Tracking
Fundamentally, agile is about delivering value early and often, getting feedback, and making iterative improvements. In 2Q21 we are introducing outcome tracking into the LRP release planning process. Identifying the expected benefits tied to Epics, provides the delivery team additional information that can be used to compare Epics, prioritize work, and make trade-off decisions based on benefit delivery. Delivery teams will use this information to prioritize, track progress, and measure the impact of their work.


### LRP Outcome Capture
Guidance for Business Outcomes is available here.

| Dimension | Variable | Business Outcome | Epic | Community OKR (Baseline) | Community OKR (Target) |
| --------- | -------- | ---------------- | ---- | ------------------------ | ---------------------- |
| Market Differentiation & Competitive Advantage | Product Evolution | Establish a modern mechanism for data sharing to internal and external consumers, providing speed close to real-time, standardization and flexibility. | 1, 3 | NPS: Current<br><br>Market Share: Current 25%<br><br>Enable of new business models<br><br>Be the preferred asset manager solution for non-competitors | NPS: +X%<br><br>Market Share: +X% |
| Market & Business evolution | Product Roadmap | Enable the flexibility to business layer to implement a new or remove specialized packages for funds asset management, maintaining the transparency and standardization to the services' consumers. | 1, 2 | Market Share: Current 25%<br><br>Adopt new Specialized Packages<br><br>Enable of new business models<br><br>Be the preferred asset manager solution for non-competitors | Market Share: +X% |
| Customer Satisfaction | Product Performance | Increased services performance and resilience, being able to support the new consumer profile expected and delivering a high level experience in terms of stability and trust to internal and external customers and customer's client. | 2, 3, 4 | NPS: Current<br><br>Market Share: Current 25%<br><br>Be the preferred asset manager solution for non-competitors | NPS: +X%<br><br>Market Share: +X% |
| Market & Business evolution | Product Roadmap | Reduced coupling to increase the velocity to deploy new versions and speed-up business demands supporting the pace of evolution requested in the market. | 2, 3 | Market Share: Current 25%<br><br>Enable of new business models<br><br>Be the preferred asset manager solution for non-competitors | Market Share: +X% |
| Operational Excellence | Cost Reduction | Guarantee the best utilization of technical resources to assure cost efficiency for the platform and reduce complexity to maintain the platform, enabling observability on technical and functional layer to enable preventive actions and speed-up problem solving. | 4 | NPS: Current<br><br>Increase of operating margin in dollar amount | NPS: +X% |


## Epics Description
| Epics | Priority | Description | Definition of Done (LRP) |
| ----- | -------- | ----------- | ------------------------ |
| <ol start="1"><li>Anti-Corruption Layer - Discovery</li></ol> | Medium | Epic to handle the discovery of current implementation of Sigla EZ9 and their interfaces, including OC3 and Specialized Packages. | <ul><li>AS-IS Documentation</li><li>Quick wins adjustments</li></ul> |
| <ol start="2"><li>Anti-Corruption Layer - Input</li></ol> | High | Epic for review of existing input of anti-corruption layer with current SAC and AMPLIS and define a patterns for new attachments for other packages. | <ul><li>TO-BE Architecture for specialized packages interfaces</li></ul> |
| <ol start="3"><li>Anti-Corruption Layer - Output</li></ol> | High | Epic for review of existing output of anti-corruption layer built with Java and Node.js on top of Openshift (to be migrated to AWS). | <ul><li>TO-BE Architecture for output interfaces</li></ul> |
| <ol start="4"><li>Anti-Corruption Layer - Operations</li></ol> | Medium | Epic to define a model and patterns to be used for the development of platform and its CI/CD pipeline and testing services.<br><br>Include the review of the platforms based on the perspective of operational excellence for cost optimization and observability. | <ul><li>Practices and Adoption Patterns for DevOps</li><li>Practices and Adoption Patterns for Observability</li><li>Recomendations and Practices for Cost Optmization</li></ul> |


## Roadmap
| Sprint | Start Date | End Date | Vacation / Holiday | Scope | Sprint Outcomes |
| ------ | ---------- | -------- | ------------------ | ----- | --------------- |
| 27 | 01/11/2021 | 19/11/2021 | Finados (02-11)<br><br>República (15-11) | **Discovery Phase:**<br><ul><li>dissect OC3... List of modules and components... follow list and identify underlying Legacy Systems. Work in Layers. Legacy, Clandestinos, Gap analysis. But also determine what transactions or interfaces were included to Ativos and should not be there in order to start fresh with an organized architecture. There is lack of documentation. Functions were implemented as response to one-off needs.</li><li>Identification and Prioritization of functions that needs to be migrated from OC3 to EZ9 (Ativos Functions)</li><li>Dicovery the requirements for the Chinese wall data segregation</li><li>Understand of how "Aviso PaC" works currently</li><li>Review and Documentation of current architecture of EZ9 (Hom, Dev - currently in AWS, PRD - currently in PaaS)</li><li>Review and Documentation of Input and Output layers of "camada"</li><li>Understand of current operation model (Dev and Ops)</li><li>Understand of current Observability model</li><li>Integration with Passivos team to work in a common vision of platform usage</li></ul> | <ol><li>Process to identify Ativos functions and no ativos functions. (Chanki/Ligia/Williams)</li><li>List of prioritized functions.</li><li>Map out dev backlog in terms of: modernization, new functions, and Kafka infrastructure.</li><li>Current State documentation and Architecture for OC3 and EZ9.</li><li>Current State for DevOps</li><li>Current State for Observability</li></ol> |
| 28 | 22/11/2021 | 10/12/2021 | - | **Planning Phase:**<br><ul><li>Definition of a migration plan/blueprint to the prioritized functions</li><li>Definition/Review of solution/blueprint to a new model of "Eventos PaC" (old Aviso PaC).</li><li>Definition of TO-BE/blueprint architecture of EZ9 (all environments in AWS)</li><li>MUST TO HAVE: Best Practices definitions for the Input / Output layer</li><li>MUST TO HAVE: Chinese wall for data segregation architecture</li><li>NICE TO HAVE: Data Lake integration model</li><li>Blueprint for DevOps (CI/CD Pipeline, observability). | <ol><li>Migration Plan for selected Functions</li><li>To-BE for new "Eventos PaC"</li><li>To-BE Architecture for EZ9</li><li>Blueprint for DevOps</li></ol> |
| 29 | 13/12/2021 | 31/12/2021 | Christmas (24-12)<br><br>New Years (31-12) | **Development Phase:**<br><ul><li>Build a PoC for the approved designs</li><li>Build a PoC for DevOps</li><li>Build a PoC for Observability</li><li>Generate the backlog for Developers (use cases, stories, functional requirements) to implementation of functions on EZ9</li><li>Prepare the access to the Developers</li></ul> | <ol><li>Model ready to be implemented</li><li>Environment ready and backlog for Developers</li></ol> |

     
### Decisions
__Capture decisions made or identified during the LRP. Identified decisions are future decisions that team will need to make to move forward.__

| #   | Decision | Status | Owner | Impact | Due date |
| --- | -------- | ------ | ----- | ------ | -------- |
| 1 | Functions to be Prioritized | Open | Diego/Nomura | | 12/11/2021 |
| 2 | Define the amount of Functions to be worked in this Release | Open | Leonardo/Diego/Lili | | End of Sprint 1<br>19/11/2021 |


### Risks & Blocker
__Identify risks or blockers raised by the team during the LRP. Use the SIOAT format to capture and present information.__
- **SITUATION:** Collaboration with TOTVS provider and information about SAC and AMPLIS
  - IMPACT: Delay on support or requests to understand and to run the discovery phase
  - ACTION:
  - OWNER: Diego
  - TARGET DATE:
- **SITUATION:** Openstack Licensing (IaaS) and setting in cloud environment - migration planned
  - IMPACT: No clarity to define the TO-BE arquitectures and delay on deliverables.
  - ACTION:
  - OWNER: TBD
  - TARGET DATE:
- **SITUATION:** Integration of Information between the on-premises and AWS domains
  - IMPACT: No possiblity to define TO_BE and to deploy the PoC planned.
  - ACTION:
  - OWNER:
  - TARGET DATE:
- **SITUATION:** Lack of documentation or access on platforms in scope of discovery
  - IMPACT: Delay on support or requests to understand and to run the discovery phase.
  - ACTION:
  - OWNER: Diego
  - TARGET DATE:
- **SITUATION:** Data access between AWS (DB) and Iaas/PaaS (DB) are not allowed (work on exceptions only and with approval) 
  - IMPACT: No possiblity to define TO_BE and to deploy the PoC planned.
  - ACTION:
  - OWNER: TBD
  - TARGET DATE:


### Dependencies
What work streams are dependent on your work?
- Prioritization of Functions to be migrated
- NAV Team dependency to validate teh SAC/AMPLIS changes
- TOTVS Support
- Other RTs in the program - RT Experiencia do Usuário (PoD 23 and 24)
- Cloud Foundation and Cloud Products teams (enable services/products that can be used by this PoD)
- Security Workstreams
- Full catalog of acceptable Ativos functions.

Tooling - What tooling will this work stream need in order to execute work? 
- Openshift
- SAC
- AMPLIS


### Resources / Inputs
What resources are required to support the team’s work?
- Existing AWS Accounts, Landing Zones and Support access 
- IU+DevOps team suport and guiding

What inputs are required to support the team's work?
- Informations about Specialized Packages
- Integration information on Anti-Corruption Layer (In and Out interfaces)

## Fist of Five Results
Round 1
