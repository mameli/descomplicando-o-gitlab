###################################
##### Configuracoes do Mameli #####
###################################

# Configuracao do PS1
#--------------------
#PS1='\[\033]0;$TITLEPREFIX:$PWD\007\]\n\[\033[32m\]\u@\h \[\033[35m\]$MSYSTEM \[\033[33m\]\w\[\033[36m\]`__git_ps1`\[\033[0m\]\n$ '
PS1='\[\033]0;Git Bash do Mameli -> $PWD\007\]\n\[\033[32m\]Mameli@Notebook:\[\033[33m\]\w\[\033[36m\]`__git_ps1`\[\033[0m\]\n$ '

# Configuracao de aliases
#------------------------
alias c='clear'
alias rm='rm -i'
alias mv='mv -i'
